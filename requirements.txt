Django==2.1.4
django-crispy-forms==1.7.2
django-taggit==0.23.0
EbookLib==0.17
lxml==4.2.5
Pillow==5.3.0
pytz==2018.7
six==1.12.0
