from django.shortcuts import render

from django.views.generic import CreateView, ListView

import ebooklib
from ebooklib import epub

from .models import BookMeta, BookFile

class BookCreateView(CreateView):
    model = BookMeta
    fields = ('__all__')

class BookListView(ListView):
    model = BookMeta
    
class BookUploadView(CreateView):
    model = BookFile
    fields = ('book',)
    
    def form_valid(self, form):
        print('Here')
        bookfile = form.save(commit=False)
        book = epub.read_epub(bookfile.book.path)
        print(book.get_metadata('DC', 'title'))