from django.db import models

from taggit.managers import TaggableManager

# Create your models here.
class BookMeta(models.Model):
    title = models.CharField(max_length=200)
    series = models.CharField(max_length=200)
    series_number = models.DecimalField(max_digits=5, decimal_places=2)
    cover = models.ImageField()
    tags = TaggableManager()
    date_added = models.DateField(auto_now_add=True)
    date_published = models.DateField()
    
    
class Author(models.Model):
    name = models.CharField(max_length=200)
    books = models.ManyToManyField('BookMeta')
    
class Publisher(models.Model):
    name = models.CharField(max_length=200)
    books = models.ManyToManyField('BookMeta')
    
class Language(models.Model):
    name = models.CharField(max_length=200)
    books = models.ManyToManyField('BookMeta')
    
class BookFile(models.Model):
    book = models.FileField()
    filetype = models.CharField(max_length=50)
    bookmeta = models.ForeignKey('BookMeta', on_delete=models.CASCADE)
    
class BookID(models.Model):
    bookmeta = models.ForeignKey('BookMeta', on_delete=models.CASCADE)
    bookid = models.CharField(max_length=200)
    